let userName = prompt( 'What is your name?' );
let userAge = parseInt( prompt( 'How old are you?' ));
while (!userName || !userAge || userAge <= 0) {
    userName = prompt( 'What is your name?' );
    userAge = parseInt( prompt( 'How old are you?' ));
}
if (userAge < 18) {
    alert('You are not allowed to visit this website');
} else if (userAge <= 22) {
    const userAnswer = confirm('Are you sure you want to continue?');
    if (userAnswer) {
        alert(`Welcome, ${userName}`);
    } else {
        alert('You are not allowed to visit this website');
    }
} else {
    alert(`Welcome, ${userName}`);
}